package main;

import java.util.InputMismatchException;

import vehiculos.Motor;

public class PruebaMotor {

	public static void main(String[] args) {
		
		try{
			Motor motor = new Motor();
			motor.rodar(20000);
			motor.print();
			motor.println();
		}catch (IllegalArgumentException e) {
			System.out.println("Error: Se debe de correr un kilometraje positivo");		
		}
		catch (IllegalStateException ee) {
			System.out.println("Error: El motor no puede rodar mas kilometros");
		}
	}

}
