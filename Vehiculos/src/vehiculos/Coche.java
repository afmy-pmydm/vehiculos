package vehiculos;

public class Coche extends Vehiculo {
	
		private Rueda rueda[] = new Rueda[4];
		
		public Coche(Motor motor, String matricula) {
			super(motor, matricula);
			rueda[0] = new Rueda();
			rueda[1] = new Rueda();
			rueda[2] = new Rueda();
			rueda[3] = new Rueda();
			}

		@Override
		public void reparar() {
			rueda[0].reparar();
			rueda[1].reparar();
			rueda[2].reparar(); 
			rueda[3].reparar();
		}
		
		@Override
		public void rodar(int km) {
			super.rodar(km);
			rueda[0].rodar(km);
			rueda[1].rodar(km);
			rueda[2].rodar(km); 
			rueda[3].rodar(km);
		}
		
		@Override
		public String toString() {
			return super.toString() + rueda[0].toString() + rueda[1].toString() + rueda[2].toString() + rueda[3].toString();
		}

}

